# Intro

this repo is to contribute to the static page of the spanish hackmeeting website

https://es.hackmeeting.org

# ML

to participate to the mailing list you can visit this page

https://listas.sindominio.net/mailman/listinfo/hackmeeting

or you can enter to Matrix group: 

 #hackmeeting:matrix.org

# Contribute

if you want to contribute chaning the website you need to 
1. have an account on lattuga 
2. know some HTML and CSS (basic)
3. install and use git to push your changes
 
Once you have that, then proceeed with the following steps to update the file:

    git clone https://git.lattuga.net/samba/eshackmeeting.git
    # edit some file and save
    git add FILENAME
    git commit -m "update FILENAME"
    git push
    
the changes, once approved in the master branch, will be applied on the website in 30m. 


please, remember to inform the ML about the change.

